<?php

namespace common\models\database\base;

use Yii;

/**
 * This is the model class for table "call_records".
 *
 * @property string $id
 * @property string $caller
 * @property string $callee
 * @property string $start_date
 * @property string $duration
 */
class CallRecordsBase extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'call_records';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['caller', 'callee', 'start_date', 'duration'], 'required'],
            [['start_date'], 'safe'],
            [['duration'], 'integer'],
            [['caller', 'callee'], 'string', 'max' => 15]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'caller' => 'Caller',
            'callee' => 'Callee',
            'start_date' => 'Start Date',
            'duration' => 'Duration',
        ];
    }
}
