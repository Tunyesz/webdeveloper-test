<?php

namespace common\models\database;

/**
 * @inheritdoc
 */
class Role {
    const ID_OPERATOR = 1;
    const ID_EXTENSION = 2;
    const NAME_OPERATOR = 'operator';
    const NAME_EXTENSION = 'extension';
}
