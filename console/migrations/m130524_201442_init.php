<?php

use yii\db\Schema;
use yii\db\Migration;

class m130524_201442_init extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%user}}', [
            'id' => Schema::TYPE_PK,
            'username' => Schema::TYPE_STRING . ' NOT NULL UNIQUE',
            'full_name' => Schema::TYPE_STRING . '(150) COLLATE utf8_unicode_ci NOT NULL',
            'auth_key' => Schema::TYPE_STRING . '(32) NOT NULL',
            'password_hash' => Schema::TYPE_STRING . ' NOT NULL',
            'password_reset_token' => Schema::TYPE_STRING . ' UNIQUE',
            'email' => Schema::TYPE_STRING . ' NOT NULL UNIQUE',

            'status' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 10',
            'created_at' => Schema::TYPE_INTEGER . ' NOT NULL',
            'updated_at' => Schema::TYPE_INTEGER . ' NOT NULL',
            'role_id' => Schema::TYPE_SMALLINT ."(6) NOT NULL DEFAULT '2'"
        ], $tableOptions);
        
        $this->insert('user', [
            'username' => 'operator',
            'full_name' => 'Operator',
            'auth_key' => 'mDT15ftLiHOHMrsJH3AY6j4stvGeffS0',
            'password_hash' => '$2y$13$vUvii7tCXUUSh43eUortoOZ3ASY5w/k3irAxzlEe8pcvL81Iga6zK',
            'email' => 'operator@operator.com',
            'status' => 10,
            'updated_at' => 1433100854,
            'created_at' => 1433100854,
            'role_id' => 1
        ]);
        
        $this->createTable('role', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . '(100) COLLATE utf8_unicode_ci NOT NULL',
        ]);
        
        $this->insert('role', [
            'id' => 1,
            'name' => 'operator'
        ]);
        $this->insert('role', [
            'id' => 2,
            'name' => 'extension'            
        ]);
        
        $this->createTable('call_records', [
            'id' => Schema::TYPE_PK,
            'caller' => Schema::TYPE_STRING . '(15) COLLATE utf8_unicode_ci NOT NULL',
            'callee' => Schema::TYPE_STRING . '(15) COLLATE utf8_unicode_ci NOT NULL',
            'start_date' => Schema::TYPE_DATETIME . ' NOT NULL',
            'duration' => Schema::TYPE_INTEGER . ' unsigned NOT NULL'
        ]);

    }

    public function down()
    {
        $this->dropTable('{{%user}}');
        $this->dropTable('{{%role}}');
        $this->dropTable('{{%call_records}}');
    }
}
