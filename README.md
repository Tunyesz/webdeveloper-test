#INSTALLATION

1. With Composer installed, you can install the application using the following commands:

        composer global require "fxp/composer-asset-plugin:1.0.0"
        composer install

2. Execute the `init` command and select `dev` as environment.

        php /path/to/yii-application/init

3. Create a new database and adjust the `components['db']` configuration in `common/config/main-local.php` accordingly.
4. Apply migrations with console command `yii migrate`.
5. Set document roots of your web server:
        for frontend `` `/path/to/yii-application/frontend/web/` `` and using the URL `` `http://frontend/`` `



DIRECTORY STRUCTURE
-------------------

```
common
    config/              contains shared configurations
    mail/                contains view files for e-mails
    models/              contains model classes used in both backend and frontend
console
    config/              contains console configurations
    controllers/         contains console controllers (commands)
    migrations/          contains database migrations
    models/              contains console-specific model classes
    runtime/             contains files generated during runtime
frontend
    assets/              contains application assets such as JavaScript and CSS
    config/              contains frontend configurations
    controllers/         contains Web controller classes
    models/              contains frontend-specific model classes
    runtime/             contains files generated during runtime
    views/               contains view files for the Web application
    web/                 contains the entry script and Web resources
    widgets/             contains frontend widgets
vendor/                  contains dependent 3rd-party packages
environments/            contains environment-based overrides
tests                    contains various tests for the advanced application
    codeception/         contains tests developed with Codeception PHP Testing Framework
```
