<?php

namespace frontend\controllers;

use common\models\database\CallRecords;
use common\models\database\Role;
use common\models\database\User;
use frontend\models\SignupForm;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * UserController implements the CRUD actions for User model
 * (for the extension) and the list for call-records.
 */
class UserController extends Controller {

    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    // Specify the available actions for an extension
                    [
                        'actions' => ['call-records', 'update'],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return @Yii::$app->user->getIdentity()->role_id == Role::ID_EXTENSION;
                        }
                    ],
                    // Specify the available actions for an operatior
                    [
                        'actions' => ['index', 'create', 'update', 'delete', 'call-records'],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return @Yii::$app->user->getIdentity()->role_id == Role::ID_OPERATOR;
                        }
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Extensions.
     * @return mixed
     */
    public function actionIndex() {
        $dataProvider = new ActiveDataProvider([
            'query' => User::find()->where(
                    'status=:status AND role_id=:roleID',
                    [
                        ':status' => User::STATUS_ACTIVE,
                        ':roleID' => Role::ID_EXTENSION
                    ]
            ),
        ]);

        return $this->render('index', [
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Extension's all call records.
     * @param integer $id
     * @return mixed
     */
    public function actionCallRecords($id) {
        if (
            @Yii::$app->user->getIdentity()->role_id == Role::ID_EXTENSION
            && Yii::$app->user->id != $id
        ) {
            throw new \yii\web\UnauthorizedHttpException();
        }

        $user = $this->findModel($id);

        $dataProvider = new ActiveDataProvider([
            'query' => CallRecords::find()->where(
                    'caller=:username OR callee=:username',
                    [
                        ':username' => $user->username,
                    ]
            ),
        ]);

        return $this->render('call-records', [
                    'dataProvider' => $dataProvider,
                    'user' => $user
        ]);
    }

    /**
     * Creates a new Extension.
     * If creation is successful, the browser will be redirected to the 'extension list' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                Yii::$app->getSession()->setFlash('success', 'Extension successfully created');
                return $this->redirect(['user/index']);
            }
        }

        return $this->render('//site/signup', [
                    'model' => $model,
        ]);

    }

    /**
     * Updates an existing Extension.
     * If update is successful, the browser will be redirected to the 'extension list' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        // Deny access from the current logined extension
        // if the update request not belongs to him/her
        if (
            @Yii::$app->user->getIdentity()->role_id == Role::ID_EXTENSION
            && Yii::$app->user->id != $id
        ) {
            throw new \yii\web\UnauthorizedHttpException();
        }
        
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->getSession()->setFlash('success', 'Successfull update.');
            // Don't redirect in case of current user is an extension
            switch (Yii::$app->user->getIdentity()->role_id) {
                case Role::ID_EXTENSION:
                    break;
                default: 
                    return $this->redirect(['index']);
                    break;
            }
            
        }
        
        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Extension.
     * (Changes the status of the user to deleted)
     * If deletion is successful, the browser will be redirected to the 'extension list' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $model = $this->findModel($id);
        $model->status = User::STATUS_DELETED;
        if ($model->save()) {
            Yii::$app->getSession()->setFlash('success', 'Successfull delete.');
        } else {
            Yii::$app->session->setFlash('error', 'There was an error deleting extension.');
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the Extension (User) based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        $model = User::find()->where(
                        'id=:ID AND status=:status AND role_id=:roleID', [
                            ':ID' => $id,
                            ':status' => User::STATUS_ACTIVE,
                            ':roleID' => Role::ID_EXTENSION
                        ]
                )->one();

        if ($model !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
