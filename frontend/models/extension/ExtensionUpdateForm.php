<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace frontend\models\extension;

use yii\base\Model;

/**
 * Description of ExtensionUpdateForm
 *
 * @author Tunyesz
 */
class ExtensionUpdateForm extends Model{
    public $email;
    public $address;
    public $fullName;
    
    
    /**
     * Create user and extension record into the database
     * @return boolean
     */
    public function update() {
        if ($user = $this->createUser()) {
            if ($extension = $this->createExtension($user)) {
                return $extension;
            }
        } else {
            return false;
        }
    }
}
