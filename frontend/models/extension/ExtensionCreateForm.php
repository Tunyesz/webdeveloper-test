<?php

namespace frontend\models\extension;

use common\models\database\Extension;
use common\models\database\Role;
use common\models\database\User;
use yii\base\Model;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ExtensionCreateForm
 *
 * @author Tunyesz
 */
class ExtensionCreateForm extends Model {

    public $username;
    public $email;
    public $password;
    public $address;
    public $fullName;

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            ['username', 'filter', 'filter' => 'trim'],
            ['username', 'required'],
            ['username', 'unique', 'targetClass' => '\common\models\database\User', 'message' => 'This username has already been taken.'],
            ['username', 'string', 'min' => 2, 'max' => 255],
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'unique', 'targetClass' => '\common\models\database\User', 'message' => 'This email address has already been taken.'],
            ['password', 'required'],
            ['password', 'string', 'min' => 6],
            [['address'], 'string'],
            [['fullName'], 'string', 'max' => 255],
        ];
    }

    /**
     * Insert user record into the database
     * @return User
     */
    protected function createUser() {
        $user = new User();

        $user->username = $this->username;
        $user->email = $this->email;
        $user->role_id = Role::ID_EXTENSION;
        $user->setPassword($this->password);
        $user->generateAuthKey();
        if ($user->save()) {
            return $user;
        } else {
            $this->addErrors($user->getErrors());
            return false;
        }
    }

    /**
     * Insert extension record into the database
     * @param User $user
     * @return Extension
     */
    protected function createExtension(User $user) {
        $extension = new Extension();
        $extension->user_id = $user->getPrimaryKey();
        $extension->full_name = $this->fullName;
        $extension->address = $this->address;
        if ($extension->save()) {
            return $extension;
        } else {
            $this->addErrors($extension->getErrors());
            return false;
        }
    }

    /**
     * Create user and extension record into the database
     * @return boolean
     */
    public function create() {
        if ($user = $this->createUser()) {
            if ($extension = $this->createExtension($user)) {
                return $extension;
            }
        } else {
            return false;
        }
    }

}
