<?php
/* @var $this yii\web\View */
$this->title = 'My Yii Application';
?>
<div class="site-index">
    <div class="jumbotron">
        <h1>Congratulations!</h1>

        <p class="lead">Login with "operator" username and use the "operator" password</p>

        <p><a class="btn btn-lg btn-success" href="<?=yii\helpers\Url::to(['site/login'])?>">Log in</a></p>
    </div>
</div>
