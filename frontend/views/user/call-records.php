<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $user \common\models\database\User */

$this->title = Yii::t('app', 'Call records of '.$user->username);
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-call-records">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'start_date',
            'caller',
            'callee',
            'duration',
        ],
    ]); ?>

</div>
